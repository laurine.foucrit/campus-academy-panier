<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PrestaShop\CircuitBreaker\SimpleCircuitBreakerFactory;
use PrestaShop\CircuitBreaker\FactorySettings;

use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\ServiceBus\Internal\IServiceBus;
use WindowsAzure\ServiceBus\Models\BrokeredMessage;
use WindowsAzure\ServiceBus\Models\QueueInfo;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(): Response {
    	$urlApi = "http://localhost/campus-academy/public/api/livres";
		$livres = [];
	
		/** Appel API Curl */
        //Avant mise en place du circuit breaker, l'appel se fait maintenant avec circuit breaker
        /*
		// create & initialize a curl session
		$curl = curl_init();

		// set our url with curl_setopt()
		curl_setopt($curl, CURLOPT_URL, $urlApi);
		// return the transfer as a string, also with setopt()
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		// curl_exec() executes the started curl session
		// $output contains the output string
		$output = curl_exec($curl);
		$arrayLivres = json_decode($output, true)["hydra:member"];
	
		foreach ($arrayLivres as $key => $livre) {
			$datePublication = explode('T',$livre["datePublication"])[0];
			$curlAuteurUrl = "http://localhost" . $livre["auteur"];
			curl_setopt($curl, CURLOPT_URL, $curlAuteurUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$outputAuteur = curl_exec($curl);
			$auteur = json_decode($outputAuteur, true);

			$livres[] = [
				"id" => $livre["id"],
				"nom" => $livre["nom"],
				"auteur" => $auteur["nom"] . " " . $auteur["prenom"],
				"datePublication" => $datePublication,
				"prix" => $livre["prix"],
				"stock" => $livre["stock"],
			];
		}

		// close curl resource to free up system resources
		// (deletes the variable made by curl_init)
		curl_close($curl);
		*/

        /** Event sourcing */
        ////////////// [yourNamespace] & [Primary Key] => à définir //////////////
        /// Tentative de mise en place de Azure messageQueue non finalisée
        $connectionString = "Endpoint=[yourNamespace].servicebus.windows.net;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=[Primary Key]";

        // Create Service Bus REST proxy.
        $serviceBusRestProxy = ServicesBuilder::getInstance()->createServiceBusService($connectionString);

        if(!$serviceBusRestProxy instanceof IServiceBus){
            throw new \ErrorException("Echec instance service bus");
        }
        try {
            $queueInfo = new QueueInfo("myqueue");
            // Create queue.
            $serviceBusRestProxy->createQueue($queueInfo);

            // Create message.
            $message = new BrokeredMessage();
            $message->setBody("my message");
            // Send message.
            $serviceBusRestProxy->sendQueueMessage("myqueue", $message);
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // https://docs.microsoft.com/rest/api/storageservices/Common-REST-API-Error-Codes
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }

        /** Circuit Breaker */
        // Fonctionnement ok
		$circuitBreakerFactory = new SimpleCircuitBreakerFactory();
		$circuitBreaker = $circuitBreakerFactory->create(new FactorySettings(2, 10, 1));
	
		$fallbackResponse = function () {
			return 'Service indisponible';
		};
	
		$response = $circuitBreaker->call($urlApi, [], $fallbackResponse);
		if($response == "Service indisponible"){
			throw new \ErrorException($response);
		}
		$arrayLivres = json_decode($response, true)["hydra:member"];
 		foreach ($arrayLivres as $key => $livre) {
 		
			$datePublication = explode('T',$livre["datePublication"])[0];
			$urlAuteur = "http://localhost" . $livre["auteur"];
			$fallbackResponse = function () {
				return 'Service indisponible';
			};
		 
			$responseAuteur = $circuitBreaker->call($urlAuteur, [], $fallbackResponse);
			if($responseAuteur == "Service indisponible"){
				throw new \ErrorException($response);
			}
			$auteur = json_decode($responseAuteur, true);
		
			$livres[] = [
				"id" => $livre["id"],
				"nom" => $livre["nom"],
				"auteur" => $auteur["nom"] . " " . $auteur["prenom"],
				"datePublication" => $datePublication,
				"prix" => $livre["prix"],
				"stock" => $livre["stock"],
			];
		}
		
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
			'livres' => $livres
        ]);
    }
}
