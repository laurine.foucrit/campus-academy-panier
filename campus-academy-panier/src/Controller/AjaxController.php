<?php

namespace App\Controller;

use PrestaShop\CircuitBreaker\FactorySettings;
use PrestaShop\CircuitBreaker\SimpleCircuitBreakerFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * @Route("/reservation", name="reservation_livre", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
	public function reservation(Request $request): JsonResponse
	{
		$id = (int) $request->request->get('id');
		$stock = (int) $request->request->get('stock') - 1;
		$urlApi = "http://localhost/campus-academy/api/livres/" . $id;
		/*
		$ch = curl_init($url);
		$data = json_encode(array("stock" => $stock));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$response = curl_exec($ch);

		if(curl_error($ch)){
			return new JsonResponse(['res' => $ch]);
		}
		return new JsonResponse(['res' => true]);
		*/
		try {
			$circuitBreakerFactory = new SimpleCircuitBreakerFactory();
			$settings = new FactorySettings(2, 10, 1);
			$settings->setClientOptions(['method' => 'PUT']);
			$circuitBreaker = $circuitBreakerFactory->create($settings);
			
			$fallbackResponse = function () {
				return false;
			};
			
			$data = [
				'json' => ['stock' => $stock],
			];
			$response = $circuitBreaker->call($urlApi, $data, $fallbackResponse);
			
			if (!$response) {
				throw new \ErrorException('Service indisponible');
			}
			return new JsonResponse(['res' => true]);
		} catch (\Throwable $e){
			return new JsonResponse(['res' => false, 'error' => $e->getMessage()]);
		}
	}
	
	/**
	 * @Route("/return", name="return_livre")
	 * @param Request $request
	 * @return Response
	 */
	public function return(Request $request): Response
	{	$id = (int) $request->request->get('id');
		$stock = (int) $request->request->get('stock') + 1;
		$urlApi = "http://localhost/campus-academy/api/livres/" . $id;
		
		/*
		$data = json_encode(array("stock" => $stock));
		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		
		$response = curl_exec($ch);
		
		if(curl_error($ch)){
			return new JsonResponse(['res' => $ch]);
		}
		return new JsonResponse(['res' => true]);
		*/
		try {
			$circuitBreakerFactory = new SimpleCircuitBreakerFactory();
			$settings = new FactorySettings(2, 10, 1);
			$settings->setClientOptions(['method' => 'PUT']);
			$circuitBreaker = $circuitBreakerFactory->create($settings);
			
			$fallbackResponse = function () {
				return false;
			};
			
			$data = [
				'json' => ['stock' => $stock],
			];
			$response = $circuitBreaker->call($urlApi, $data, $fallbackResponse);
			
			if (!$response) {
				throw new \ErrorException('Service indisponible');
			}
			return new JsonResponse(['res' => true]);
		} catch (\Throwable $e){
			return new JsonResponse(['res' => false, 'error' => $e->getMessage()]);
		}
	}
}
